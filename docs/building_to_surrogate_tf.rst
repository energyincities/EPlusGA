#################
Example Work Flow
#################

.. warning::

  To run this example you will need to install TensorFlow which is >300mb library. To install run ``pip install tensorflow``

.. note::
  Lots of other great surrogate modelling examples can be found `here <https://gitlab.com/energyincities/besos-examples/-/blob/master/besos/examples/SurrogateModelling/Overview.ipynb>`_ that make use of scikit learn instead of TensorFlow.


In this notebook we will go over a ensemble model made possible by the besos software.
We will create a surrogate model of a parameterized EnergyPlus building model. The model is
changed based on a given window to wall ratio and solar gain coefficient. These
variables will change the daily electricity use of the buildings. We will create a dataset
of different buildings that will contain the variations of parameters and a time series
of daily electricity use. Using this dataset we will train a neural network to
quickly find those daily electricity values without the need to rerun the EnergyPlus model.
Finally we will use this surrogate model to explore the impact of the variables using
a parallel coordinates plot. To complete this we will walk through nine steps as seen
in the figure below.

.. figure:: images/flow_diagram.PNG
   :alt: Image
    Figure 1: In this figure we have presented the general pipeline that we will use.



First we will import all the different libraries we need.

.. code-block:: python3

    #!pip install besos --user
    %matplotlib inline

    import pandas as pd
    import numpy as np
    import tensorflow as tf
    import tensorflow.keras as keras
    from tensorflow.keras import layers
    from besos import eppy_funcs as ef
    import besos.sampling as sampling
    from besos.problem import EPProblem
    from besos.evaluator import EvaluatorEP
    from besos.parameters import wwr, RangeParameter,FieldSelector, Parameter
    import tensorflow_docs as tfdocs
    import tensorflow_docs.plots
    import tensorflow_docs.modeling
    import time
    from dask.distributed import Client


.. code-block:: python3

    # Use seaborn for pairplot
    #!pip install --upgrade tensorflow --user

    # Use some functions from tensorflow_docs
    #!pip install git+https://github.com/tensorflow/docs --user


(1) Set up the building from idf
================================
The building is defined by the Information Data File (IDF) or using the
new EnergyPlus format (epJSON).

.. code-block:: python3

    # Open the IDF file
    building = ef.get_building("./examples/EnsembleWorkflows/Medium_Office.idf")
    building.view_model()



.. image:: images/output_5_0.png


.. code-block:: python3

    #You can convert an idf to epJSON using the following code.
    # !energyplus -c "/examples/EnsembleWorkflows/Medium_Office.idf"

(2) Evaluator
=============

Set up the inputs and outputs of your exploration
-------------------------------------------------

Defines how we will evaluate the building; - what external weather
conditions is the building experiencing, - what properties of the
building will we be changing, and - what are some of the performance
metrics of the building that we want to explore.

The weather conditions are specified in the EnergyPlus Weather File
(EWP) file. The properties we will change in the building will be
defined in the parameter space. In the objectives we will specify the
what output performance metrics we wish to extract such that we can
explore them later.

.. code-block:: python3

    # building.idfobjects

.. code-block:: python3

    # for materials in building.idfobjects["MATERIAL:NOMASS"]:
    #     print("{} {}".format(materials.Name,materials.Thermal_Resistance))

    # for materials in building.idfobjects["BUILDINGSURFACE:DETAILED"]:
    #     if materials.Sun_Exposure!="NoSun": print(materials.Construction_Name )

    # for materials in building.idfobjects['CONSTRUCTION']:
    #     if materials.Name=="BTAP-Ext-Wall-Mass:U-0.315": print(materials)

.. figure:: images/setting_up_the_evaluator.PNG
   :alt: Image

   Figure 2:  Setting up the evaluator

.. code-block:: python3

    # Here we change all the external insulation of the building
    insu1 = FieldSelector(class_name='MATERIAL:NOMASS', object_name='Typical Insulation 2', field_name='Thermal Resistance')


    # Setup the parameters, Solar Heat Gain Coefficient
    parameters = [Parameter(FieldSelector('Window',"*",'Solar Heat Gain Coefficient'),
                    value_descriptor=RangeParameter(0.01,0.99),
                    name='Solar Gain Coefficient'),
                 Parameter(insu1,
                    value_descriptor=RangeParameter(1,15),
                    name='Insulation Resistance'),]


    # Add window-to-wall ratio as a parameter between 0.1 and 0.9 using a custom function
    parameters.append(wwr(RangeParameter(0.1, 0.9)))


    # Construct the objective
    objective = ['Electricity:Facility']


    # Build the problem
    problem = EPProblem(parameters, objective)

.. code-block:: python3

    # setup the evaluator
    evaluator = EvaluatorEP(problem, building, epw_file = "/examples/EnsembleWorkflows/victoria.epw", multi=True,
                            progress_bar=True, distributed=True, out_dir="outputdirectory")

(3) Generate the Dataset
========================

1. Sample the problem space
2. Setup the parallel processing
3. Generate the Samples
4. Store and recover the expensive runs

.. code-block:: python3


    # Use latin hypercube sampling to take 30 samples
    inputs = sampling.dist_sampler(sampling.lhs, problem,100)


    # sample of the inputs
    print(inputs.head())


.. parsed-literal::

       Solar Gain Coefficient  Insulation Resistance  Window to Wall Ratio
    0                0.980214               9.160124              0.650856
    1                0.661204              13.430512              0.597784
    2                0.716411               4.617035              0.337802
    3                0.518645               7.229073              0.677144
    4                0.206976              10.194319              0.808266


.. code-block:: python3

    # Setup the parallel processing in the notebook.
    client = Client(threads_per_worker=1)
    client




.. raw:: html

    <table style="border: 2px solid white;">
    <tr>
    <td style="vertical-align: top; border: 0px solid white">
    <h3 style="text-align: left;">Client</h3>
    <ul style="text-align: left; list-style: none; margin: 0; padding: 0;">
      <li><b>Scheduler: </b>tcp://127.0.0.1:37141</li>
      <li><b>Dashboard: </b><a href='/user/username/proxy/8787/status' target='_blank'>/user/username/proxy/8787/status</a>
    </ul>
    </td>
    <td style="vertical-align: top; border: 0px solid white">
    <h3 style="text-align: left;">Cluster</h3>
    <ul style="text-align: left; list-style:none; margin: 0; padding: 0;">
      <li><b>Workers: </b>16</li>
      <li><b>Cores: </b>16</li>
      <li><b>Memory: </b>34.36 GB</li>
    </ul>
    </td>
    </tr>
    </table>



Run the samples

.. code-block:: python3

    t1=time.time()
    # Run Energyplus
    outputs = evaluator.df_apply(inputs)
    t2=time.time()
    time_of_sim=t2-t1


.. parsed-literal::

    /usr/local/lib/python3.7/dist-packages/distributed/worker.py:3339: UserWarning: Large object of size 6.95 MB detected in task graph:
      ("('from_pandas-10fc1b0e937cec3185233c4f9719f146', ... 33c4f9719f146')
    Consider scattering large objects ahead of time
    with client.scatter to reduce scheduler burden and
    keep data on workers

        future = client.submit(func, big_data)    # bad

        big_future = client.scatter(big_data)     # good
        future = client.submit(func, big_future)  # good
      % (format_bytes(len(b)), s)


Calculate the time

.. code-block:: python3

    def niceformat(seconds):
        seconds = seconds % (24 * 3600)
        hour = seconds // 3600
        seconds %= 3600
        minutes = seconds // 60
        seconds %= 60
        return hour, minutes, seconds

    hours,mins,secs=niceformat(time_of_sim)

    print("The total running time: {:2.0f} hours {:2.0f} min {:2.0f} seconds".format(hours,mins,secs))
    # Build a results DataFrame

.. code-block:: python3

    results = inputs.join(outputs)
    results.head()

Take a look at the results
--------------------------

.. code-block:: python3

    total_heating_use = results['Electricity:Facility']

    def norm_res(results):
        results_normed = (results - np.mean(results))/np.std(results)
        return results_normed

    plt.scatter(norm_res(results['Solar Gain Coefficient']),total_heating_use,label="solar gain")
    plt.scatter(norm_res(results['Window to Wall Ratio']),total_heating_use,label="w2w ratio")
    plt.scatter(norm_res(results['Insulation Resistance']),total_heating_use,label="Insulation Resistance")

    plt.legend()

Store the expensive calculations
--------------------------------

Since this can quite a big run. Lets store the results such that we
don't have to rerun this problem.

.. code-block:: python3

    inputs.to_pickle("inputs.pkl")
    outputs.to_pickle("outputs.pkl")

.. code-block:: python3

    inputs_ = pd.read_pickle("inputs.pkl")
    outputs_ = pd.read_pickle("outputs.pkl")

(4) Setup the dataset for the Surrogate Model
=============================================

The outputs are packed in a single columns which will not work for
tensorflow.

.. code-block:: python3

    print(outputs_.head())
    print(inputs_.head())


.. parsed-literal::

       Electricity:Facility
    0          1.999243e+12
    1          1.966446e+12
    2          1.634494e+12
    3          1.604978e+12
    4          1.838792e+12
       Solar Gain Coefficient  Insulation Resistance  Window to Wall Ratio
    0                0.908584               4.854668              0.822617
    1                0.469324              11.766597              0.798576
    2                0.390397               7.866908              0.188102
    3                0.367993               9.632202              0.120128
    4                0.405527               9.803255              0.585378


We will repack them using the following code, to get 365 different
columns which will represent the output labels. Build the full dataset
with inputs and outputs to easily split up the train and test data sets.
The training data sets are used to train the model, while the test data
set will show how general the model is.

.. code-block:: python3

    dataset=inputs_.join(outputs_)
    dataset.head()




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }

        .dataframe tbody tr th {
            vertical-align: top;
        }

        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Solar Gain Coefficient</th>
          <th>Insulation Resistance</th>
          <th>Window to Wall Ratio</th>
          <th>Electricity:Facility</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>0.908584</td>
          <td>4.854668</td>
          <td>0.822617</td>
          <td>1.999243e+12</td>
        </tr>
        <tr>
          <th>1</th>
          <td>0.469324</td>
          <td>11.766597</td>
          <td>0.798576</td>
          <td>1.966446e+12</td>
        </tr>
        <tr>
          <th>2</th>
          <td>0.390397</td>
          <td>7.866908</td>
          <td>0.188102</td>
          <td>1.634494e+12</td>
        </tr>
        <tr>
          <th>3</th>
          <td>0.367993</td>
          <td>9.632202</td>
          <td>0.120128</td>
          <td>1.604978e+12</td>
        </tr>
        <tr>
          <th>4</th>
          <td>0.405527</td>
          <td>9.803255</td>
          <td>0.585378</td>
          <td>1.838792e+12</td>
        </tr>
      </tbody>
    </table>
    </div>



Split dataset into test and training
------------------------------------

.. code-block:: python3

    train_dataset = dataset.sample(frac=0.8, random_state=0)
    test_dataset = dataset.drop(train_dataset.index)

    training_labels = train_dataset[outputs_.columns]
    testing_labels = test_dataset[outputs_.columns]
    training_labels




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }

        .dataframe tbody tr th {
            vertical-align: top;
        }

        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Electricity:Facility</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>26</th>
          <td>1.954075e+12</td>
        </tr>
        <tr>
          <th>86</th>
          <td>1.710905e+12</td>
        </tr>
        <tr>
          <th>2</th>
          <td>1.634494e+12</td>
        </tr>
        <tr>
          <th>55</th>
          <td>1.724594e+12</td>
        </tr>
        <tr>
          <th>75</th>
          <td>1.722192e+12</td>
        </tr>
        <tr>
          <th>...</th>
          <td>...</td>
        </tr>
        <tr>
          <th>69</th>
          <td>1.724147e+12</td>
        </tr>
        <tr>
          <th>20</th>
          <td>1.949339e+12</td>
        </tr>
        <tr>
          <th>94</th>
          <td>1.921412e+12</td>
        </tr>
        <tr>
          <th>72</th>
          <td>1.758905e+12</td>
        </tr>
        <tr>
          <th>77</th>
          <td>1.885642e+12</td>
        </tr>
      </tbody>
    </table>
    <p>80 rows × 1 columns</p>
    </div>



Normalize the Data (Inputs of the model)
----------------------------------------


We will normalize the inputs and the outputs

.. code:: python3

    train_stats = train_dataset[inputs_.columns]
    train_stats = train_stats.describe()
    train_stats = train_stats.transpose()
    train_stats




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }

        .dataframe tbody tr th {
            vertical-align: top;
        }

        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>count</th>
          <th>mean</th>
          <th>std</th>
          <th>min</th>
          <th>25%</th>
          <th>50%</th>
          <th>75%</th>
          <th>max</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>Solar Gain Coefficient</th>
          <td>80.0</td>
          <td>0.519140</td>
          <td>0.269162</td>
          <td>0.015683</td>
          <td>0.302935</td>
          <td>0.527331</td>
          <td>0.735092</td>
          <td>0.988500</td>
        </tr>
        <tr>
          <th>Insulation Resistance</th>
          <td>80.0</td>
          <td>7.822454</td>
          <td>4.022222</td>
          <td>1.124960</td>
          <td>4.420202</td>
          <td>7.855031</td>
          <td>10.947344</td>
          <td>14.994939</td>
        </tr>
        <tr>
          <th>Window to Wall Ratio</th>
          <td>80.0</td>
          <td>0.508981</td>
          <td>0.222432</td>
          <td>0.111234</td>
          <td>0.332921</td>
          <td>0.510361</td>
          <td>0.697192</td>
          <td>0.885781</td>
        </tr>
      </tbody>
    </table>
    </div>



.. code-block:: python3


    # use the stats we calculated to do the normalization on the input.
    def norm_input(x):
        return (x - train_stats['mean']) / train_stats['std']

    def unnorm_input(x):
        return (x* train_stats['std'])+ train_stats['mean']

    normed_train_data = norm_input(train_dataset[inputs_.columns])
    normed_test_data = norm_input(test_dataset[inputs_.columns])


    print(test_dataset[inputs_.columns].head())
    print(normed_test_data.head())
    print(unnorm_input(normed_test_data.head()))


.. parsed-literal::

        Solar Gain Coefficient  Insulation Resistance  Window to Wall Ratio
    9                 0.807155               6.023020              0.465111
    12                0.663061              12.758043              0.541317
    21                0.865366              11.065040              0.736124
    25                0.164315               1.926658              0.242456
    36                0.251987               5.551554              0.330283
        Solar Gain Coefficient  Insulation Resistance  Window to Wall Ratio
    9                 1.070047              -0.447373             -0.197232
    12                0.534702               1.227080              0.145375
    21                1.286313               0.806168              1.021179
    25               -1.318257              -1.465806             -1.198237
    36               -0.992535              -0.564589             -0.803388
        Solar Gain Coefficient  Insulation Resistance  Window to Wall Ratio
    9                 0.807155               6.023020              0.465111
    12                0.663061              12.758043              0.541317
    21                0.865366              11.065040              0.736124
    25                0.164315               1.926658              0.242456
    36                0.251987               5.551554              0.330283


Normalize the labels (Outputs of the model)
-------------------------------------------


labels are the actual outputs that we are interested in.

.. code:: python3

    train_mean = np.mean(training_labels)
    train_std  = np.std(testing_labels)
    train_mean, train_std




.. parsed-literal::

    (Electricity:Facility    1.816284e+12
     dtype: float64,
     Electricity:Facility    1.433704e+11
     dtype: float64)



.. code-block:: python3

    def norm_output(x):
        return (x - train_mean) /train_std

    def unnorm_output(x):
        return (x*train_std)+ train_mean

    train_labels = norm_output(training_labels)
    test_labels  = norm_output(testing_labels)
    train_labels.head()




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }

        .dataframe tbody tr th {
            vertical-align: top;
        }

        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Electricity:Facility</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>26</th>
          <td>0.961087</td>
        </tr>
        <tr>
          <th>86</th>
          <td>-0.735008</td>
        </tr>
        <tr>
          <th>2</th>
          <td>-1.267970</td>
        </tr>
        <tr>
          <th>55</th>
          <td>-0.639533</td>
        </tr>
        <tr>
          <th>75</th>
          <td>-0.656285</td>
        </tr>
      </tbody>
    </table>
    </div>



(5) Build & Train Surrogate model architecture
==============================================

.. code-block:: python3

    def build_model():
        model = keras.Sequential([
           layers.Dense(5, input_shape=[len(train_dataset[inputs_.columns].keys())]),
           layers.Dense(5),
           layers.Dense(1)
        ])

        optimizer = tf.keras.optimizers.RMSprop(0.0001)

        model.compile(loss='mse',
                    optimizer=optimizer,
                    metrics=['mae', 'mse'])
        return model

.. code-block:: python3

    model = build_model()

.. code-block:: python3

    model.summary()


.. parsed-literal::

    Model: "sequential"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #
    =================================================================
    dense (Dense)                (None, 5)                 20
    _________________________________________________________________
    dense_1 (Dense)              (None, 5)                 30
    _________________________________________________________________
    dense_2 (Dense)              (None, 1)                 6
    =================================================================
    Total params: 56
    Trainable params: 56
    Non-trainable params: 0
    _________________________________________________________________


.. code-block:: python3

    EPOCHS = 1000

    history = model.fit(
      normed_train_data, train_labels,
      epochs=EPOCHS, validation_split = 0.2, verbose=0,
      callbacks=[tfdocs.modeling.EpochDots()])


.. parsed-literal::


    Epoch: 0, loss:3.4908,  mae:1.5995,  mse:3.4908,  val_loss:2.8472,  val_mae:1.4809,  val_mse:2.8472,
    ....................................................................................................
    Epoch: 100, loss:2.4005,  mae:1.3447,  mse:2.4005,  val_loss:2.0969,  val_mae:1.2491,  val_mse:2.0969,
    ....................................................................................................
    Epoch: 200, loss:1.6133,  mae:1.1169,  mse:1.6133,  val_loss:1.5025,  val_mae:1.0296,  val_mse:1.5025,
    ....................................................................................................
    Epoch: 300, loss:1.0368,  mae:0.9047,  mse:1.0368,  val_loss:1.0345,  val_mae:0.8159,  val_mse:1.0345,
    ....................................................................................................
    Epoch: 400, loss:0.6570,  mae:0.7113,  mse:0.6570,  val_loss:0.7000,  val_mae:0.6246,  val_mse:0.7000,
    ....................................................................................................
    Epoch: 500, loss:0.4305,  mae:0.5583,  mse:0.4305,  val_loss:0.4642,  val_mae:0.4929,  val_mse:0.4642,
    ....................................................................................................
    Epoch: 600, loss:0.2742,  mae:0.4388,  mse:0.2742,  val_loss:0.2843,  val_mae:0.3906,  val_mse:0.2843,
    ....................................................................................................
    Epoch: 700, loss:0.1550,  mae:0.3292,  mse:0.1550,  val_loss:0.1515,  val_mae:0.2824,  val_mse:0.1515,
    ....................................................................................................
    Epoch: 800, loss:0.0707,  mae:0.2218,  mse:0.0707,  val_loss:0.0645,  val_mae:0.1818,  val_mse:0.0645,
    ....................................................................................................
    Epoch: 900, loss:0.0242,  mae:0.1210,  mse:0.0242,  val_loss:0.0191,  val_mae:0.0999,  val_mse:0.0191,
    ....................................................................................................

.. code-block:: python3

    plotter = tfdocs.plots.HistoryPlotter(smoothing_std=2)

.. code-block:: python3


    plotter.plot({'Basic': history}, metric = "loss")
    plt.ylabel('loss')




.. parsed-literal::

    Text(0, 0.5, 'loss')




.. image:: images/output_47_1.png


(6) Surrogate Model & Validate against the Test dataset
=======================================================

.. code-block:: python3

    # See -> https://en.wikipedia.org/wiki/Coefficient_of_determination
    # R squared score:
    r_sqared_scores = []
    sum_res_s = []
    sum_tot_s = []
    y_i = test_labels.loc[test_labels.index].values
    y_m = np.mean(y_i)/y_i.size
    for i in range(len(normed_test_data)):
        x_i = normed_test_data.loc[normed_test_data.index[i]].tolist()
        f_i = model.predict([x_i])[0]
        y_i = test_labels.loc[test_labels.index[i]].values
        ss_res=(f_i-y_i)**2
        ss_tot=(y_i-y_m)**2
        sum_res_s.append(f_i)
        sum_tot_s.append(y_i)
        r_sqared_scores.append(1-ss_res/ss_tot)

    plt.scatter(sum_res_s,sum_tot_s)
    plt.xlabel("predicted values")
    plt.ylabel("test values")
    print("average R sqaured score: {}".format(np.mean(r_sqared_scores)))


.. parsed-literal::

    average R sqaured score: 0.9704462861305323



.. image:: images/output_49_1.png


(7) Sample Surrogate Model
==========================

.. code-block:: python3

    from besos.evaluator import EvaluatorGeneric

.. code-block:: python3

    def evaluation_func(ind):
        vals = norm_input(list(ind))
        output = unnorm_output(model.predict([list(vals)])[0][0])
        return ((output.values[0],),())

    GP_SM = EvaluatorGeneric(evaluation_func, problem)

.. code-block:: python3

    srinputs = sampling.dist_sampler(sampling.lhs, problem, 100)
    sroutputs = GP_SM.df_apply(srinputs)
    srresults = srinputs.join(sroutputs)
    srresults.head()



.. parsed-literal::

    HBox(children=(FloatProgress(value=0.0, description='Executing', style=ProgressStyle(description_width='initia…


.. parsed-literal::






.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }

        .dataframe tbody tr th {
            vertical-align: top;
        }

        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Solar Gain Coefficient</th>
          <th>Insulation Resistance</th>
          <th>Window to Wall Ratio</th>
          <th>Electricity:Facility</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>0.877723</td>
          <td>10.328806</td>
          <td>0.151321</td>
          <td>1.602558e+12</td>
        </tr>
        <tr>
          <th>1</th>
          <td>0.430164</td>
          <td>6.025796</td>
          <td>0.384092</td>
          <td>1.757320e+12</td>
        </tr>
        <tr>
          <th>2</th>
          <td>0.561125</td>
          <td>9.407312</td>
          <td>0.777024</td>
          <td>1.958208e+12</td>
        </tr>
        <tr>
          <th>3</th>
          <td>0.086388</td>
          <td>14.182918</td>
          <td>0.141822</td>
          <td>1.587385e+12</td>
        </tr>
        <tr>
          <th>4</th>
          <td>0.303898</td>
          <td>7.201950</td>
          <td>0.896001</td>
          <td>2.037647e+12</td>
        </tr>
      </tbody>
    </table>
    </div>



(8) Exploration
===============

.. code-block:: python3

    import plotly
    plotly.offline.init_notebook_mode(connected=True)



.. raw:: html

    <script type="text/javascript">
    window.PlotlyConfig = {MathJaxConfig: 'local'};
    if (window.MathJax) {MathJax.Hub.Config({SVG: {font: "STIX-Web"}});}
    if (typeof require !== 'undefined') {
    require.undef("plotly");
    requirejs.config({
        paths: {
            'plotly': ['https://cdn.plot.ly/plotly-latest.min']
        }
    });
    require(['plotly'], function(Plotly) {
        window._Plotly = Plotly;
    });
    }
    </script>



.. code-block:: python3

    import plotly.express as px
    fig = px.parallel_coordinates(srresults,color="Electricity:Facility", dimensions=["Window to Wall Ratio",
                    "Insulation Resistance","Solar Gain Coefficient" ,"Electricity:Facility"],
                                 color_continuous_scale=px.colors.diverging.Tealrose)
    fig.show()



.. image:: images/newplot.png

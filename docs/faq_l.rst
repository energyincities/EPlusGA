============================
FAQ Library
============================

If you have more questions please post them on the `gitlab issues <https://gitlab.com/energyincities/besos/-/issues>`_ board.


BESOS library (the Python functions for modelling)
==================================================

* **How do I determine the name of the EnergyPlus parameter that I want to modify?**

Since there is a lot of variation in .idf files, you have to inspect the file and find the line(s) to modify.

The Selectors notebook in the Evaluators folder has lots of examples of these.

* **How do I manipulate my EnergyPlus model programatically?**

BESOS building objects are actually EPPy building objects, so you can do things like:
```[materials.Name for materials in building.idfobjects["MATERIAL"]]```

`Eppy's documentation <https://eppy.readthedocs.io/en/latest/>`_ describes how to explore and modify the IDF object directly.

* **Why am I getting errors when using a custom FilterSelector?**

Make sure you match the syntax in your filter function to the building file type. If you are using an .idf file, convert it to an .epJSON file or use the idf-object syntax.

* **How do I convert an .idf file to an .epJSON file?**

In the command line, run 'energyplus -c' with your .idf file. There will be an .epJSON file in the output directory. Note that this will only work with EnergyPlus9.1 and above. Make sure to have the version of your .idf file up-to-date as well. Use the EnergyPlus IDF Version Updater if necessary. After installation of EnergyPlus locally, it can be found in EnergyPlusVx-x-x/PreProcess/IDFVersionUpdater.

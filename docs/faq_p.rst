============================
FAQ Platform
============================

If you have more questions please post them on the `gitlab issues <https://gitlab.com/energyincities/besos/-/issues>`_ board.

BESOS platform (the Jupyter interface)
======================================

* **How much computing power is provided?**

Right now, lots! We have 16 cores, 160Gb RAM and 3Tb of storage space on the cloud (thanks, `Compute Canada <https://www.computecanada.ca/>`_!).

Initially we are not imposing quotas, just asking everyone to respect the needs of other users.

If you start hogging the system, we will come and glare at you menacingly.

* **How do I download notebooks from BESOS?**

Right-click on the notebook file in the file manager window on the left and select "download".

* **How do I download my notebook as a .py file?**

Select File, Export Notebook As... and select "Executable Script"

* **How do I upload files to BESOS?**

Click the up arrow above the file manager window (make sure you're in the right directory first).

* **How do I share files between users?**

You can set up the `Google Drive extension <https://github.com/jupyterlab/jupyterlab-google-drive/blob/master/docs/setup.md>`_ for a folder that you both have access to.

* **How do I use the standard Jupyter Notebook rather than this fancy Jupyter Lab interface?**

   There are two ways to do this:
   * Through the GUI, select the "commands" (control+shift+C) icon on the side bar, and scroll down under the help heading and select the "Launch as Classic Notebook" command.
   * Go to the URL bar of the browser and replace everything after "your_username/" with "notebook" and press enter.

* **I keep getting an: typeError: unsupported operand type(s) for +=: 'float' and 'str', what is causing it and how do I fix it?**

   This error is usually caused by the presence of spaces or letters in the same excel cells as number values.  Or perhaps spaces in the cells that are left blank.  Ensure that there are no spaces and this should fix the problem.

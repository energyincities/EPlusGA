*********************************
API Reference
*********************************


Modules
===============

:doc:`modules/config` defines various constants and defaults used in the other files.

:doc:`modules/eppy_funcs` contains miscellaneous functions used to interact with the ``eppy`` package.
- Initialises idf objects
- Window adjustment helper functions
- Variable name conversions

:doc:`modules/errors` contains custom errors that Besos can throw.

:doc:`modules/evaluator` contains tools that convert parameters and their values into measurements of the properties of the building they represent.

:doc:`modules/objectives` defines the classes used to measure the building simulation and to generate output values.

:doc:`modules/optimizer` provides wrappers for the ``platypus`` and rbf_opt optimisation packages
- Performs the conversion between our Problem type and platypus' Problem type automatically.
- Converts Pandas DataFrames to populations of platypus Solutions
- Supports NSGAII, EpsMOEA, GDE3, SPEA2 and and other algorithms
- Supports rbf_opt

:doc:`modules/parameters` contains different classes used to represent the attributes of the building that can be varied, such as the thickness of the insulation, or the window to wall ratio. These parameters are separate from the value that they take on during any evaluation of the model.

:doc:`modules/problem` defines classes used to bundle the parameters, objectives and constraints, and to manage operations that involve all of them at once, such as converting data related to the problem to a DataFrame.

:doc:`modules/pyehub_funcs` provides helper functions for interacting with PyEHub.

:doc:`modules/sampling` includes functions used in selecting values for parameters in order to have good coverage of the solution space.

:doc:`modules/besostypes` consists of type definitions used for Besos' type hints.

:doc:`modules/besos_utils` consists of helper functions to be used throughout codebase


Supporting Files
================

In most cases, these files will not need to be imported by users.

:doc:`modules/dask_utils` contains dask related helper functions.

:doc:`modules/errors` defines error classes used by this module.

:doc:`modules/IDF_class` Enables some geomeppy based functionality on idf objects.

:doc:`modules/IO_Objects` defines some abstract superclasses that are used for the objects
that handle input and output of evaluators (Parameters/Objectives/Descriptors/etc).

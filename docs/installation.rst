
================
Installing BESOS
================

Ensure that you have Python 3.7+ and the corresponding version of pip.

Use ``pip install besos[complete]`` to install all the dependencies and examples.


.. tip::
    If you do not want to run the example notebooks, you can use ``pip install besos``, which will install with less dependencies.

Install Dependencies
====================
We use third party software to run building models (`EnergyPlus`), to solve EnergyHub models (a MILP solver), and make use of Rbfopt (via `Bonmin`). To use this functionality you need to install the software for the corresponding the task. GLPK and Bonmin are optional, it is possible to use Besos without them.


Install Energyplus
~~~~~~~~~~~~~~~~~~
Download EnergyPlus `here <https://energyplus.net/downloads>`_. (`BESOS` is currently supporting versions from 8.8-9.3+).

**For windows**: After downloading the installation file, double click the setup file to start installing. After setup is complete, navigate to your `System Properties` and in the `Advanced` tab, select `Environment Variables`. In either your `User Variables` or `System Variables` (Depending on your permissions), double click on `Path` and add the location of your `EnergyPlus` folder to the end of it.

**For linux**: Run the downloaded script, and accept the prompt to add symlinks.

TensorFlow (only required to use TensorFlow)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
TensorFlow is a machine learning library of which we have provided some examples. This package is quite large 300 mb therefore we have not added this to the requirements.

To install TensorFlow use pip install.

.. code-block:: python3

    pip install tensorflow


MILP solver (only required to use EnergyHub)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
`EnergyHub <https://gitlab.com/energyincities/python-ehub>`_ modelling requires a linear programming solver that is supported by `PuLP <https://pypi.org/project/PuLP/>`_, such as `GLPK`, `CPlex`, or `Gurobi`.

`GLPK` is free and open source. It can be found `here <https://www.gnu.org/software/glpk/>`_.
If you are using a Debian based operating system, you can install GLPK with `sudo apt install glpk-utils`

Bonmin (only required to use RBFopt)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
`Bonmin` is required to use the `RBFopt` optimizer.
How to install `Bonmin` can be found `here <https://ampl.com/products/solvers/open-source/#bonmin>`_.

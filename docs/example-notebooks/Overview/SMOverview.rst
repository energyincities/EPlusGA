Surrogate Modelling on BESOS
============================

Surrogate modelling or metamodelling is a way to emulate physics-based
building simulation models
[`1 <https://www.sciencedirect.com/science/article/pii/S0378778819302877>`__].
BESOS integrates building simulation and surrogate modelling using: +
EnergyPlus model editing and execution via EPPy using
`Evaluators <../../example-notebooks/Reference/Evaluators.html>`__ + `sampling
toolboxes <https://pythonhosted.org/pyDOE/>`__ + machine learning
toolboxes like `ScikitLearn <https://scikit-learn.org/stable/>`__ and
`TensorFlow <https://www.tensorflow.org/>`__ + the ``besos`` module to
simplify the interconnection of the above items.

Surrogate Modelling workflow
----------------------------

The figure below shows a BESOS workflow that covers all the elements
needed to derive surrogate models. This is supported by access to
powerful hardware for conducting sampling and model training quickly in
parallel.

*BESOS workflow for Surrogate Modelling
[`1 <https://www.sciencedirect.com/science/article/pii/S0378778819302877.>`__].*

The derivation of surrogate models demonstrated in the following
notebooks:

-  `Interactive Surrogate <../../example-notebooks/How-to-Guides/InteractiveSurrogate.html>`__ is a quick
   tour of a lots of BESOS: make an EnergyPlus model with two
   parameters, generate samples that span the design space, use these to
   train a surrogate model, then explore the design space using an
   interactive plot that queries the surrogate model.
-  `Fit GP Model <../../example-notebooks/How-to-Guides/FitGPModel.html>`__ makes a Gaussian Process
   surrogate model using latin hypercube sampling and the ScikitLearn
   syntax.
-  `Fit feedforward Neural
   Network <../../example-notebooks/How-to-Guides/FitFeedforwardNeuralNetwork.html>`__ makes a Neural Network
   surrogate model using latin hypercube sampling and the ScikitLearn
   syntax.
-  `Fit NN Tensorflow <../../example-notebooks/How-to-Guides/FitNNTF.html>`__ makes a Tensorflow graph and
   trains it on building simulation data.
-  `Fit GP Adaptive <../../example-notebooks/How-to-Guides/FitGPAdaptive.html>`__ adaptiveley selects
   simulations instead of picking them all in one advance. This aims to
   reduce the number of samples required to derive a surrogate model.

Others
------

-  `Parameter sets <https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/ParallelizedWorkflowsWithDask/parameter_sets.py>`__ is a .py file which defines
   some sets of besos.parameters to avoid redifining design parameters
   in each of the notebooks above.

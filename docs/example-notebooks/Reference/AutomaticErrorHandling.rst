Automatic Error Handling
========================

Sometimes there are parts of the design space that we want to explore
that will cause the EnergyPlus simulation to fail, such as invalid
combinations of parameter values.

.. code:: ipython3


    from besos import eppy_funcs as ef, sampling
    from besos.evaluator import EvaluatorEP
    from besos.parameters import (
        CategoryParameter,
        FieldSelector,
        Parameter,
        RangeParameter,
    )
    from besos.problem import EPProblem

In this example, we load the emaple model and try to use an undefined
material ``Invalid Material``.

.. code:: ipython3

    building = ef.get_building()

    problem = EPProblem(
        [
            Parameter(
                FieldSelector(
                    object_name="Mass NonRes Wall Insulation", field_name="Thickness"
                ),
                RangeParameter(min_val=0.01, max_val=0.99),
            ),
            Parameter(
                FieldSelector(
                    class_name="Construction",
                    object_name="ext-slab",
                    field_name="Outside Layer",
                ),
                CategoryParameter(options=("HW CONCRETE", "Invalid Material")),
            ),
        ]
    )

    samples = sampling.dist_sampler(sampling.lhs, problem, 5)
    samples




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }

        .dataframe tbody tr th {
            vertical-align: top;
        }

        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>RangeParameter [0.01, 0.99]</th>
          <th>Category['HW CONCRETE', 'Invalid Material']</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>0.262924</td>
          <td>HW CONCRETE</td>
        </tr>
        <tr>
          <th>1</th>
          <td>0.870820</td>
          <td>HW CONCRETE</td>
        </tr>
        <tr>
          <th>2</th>
          <td>0.735874</td>
          <td>Invalid Material</td>
        </tr>
        <tr>
          <th>3</th>
          <td>0.580238</td>
          <td>Invalid Material</td>
        </tr>
        <tr>
          <th>4</th>
          <td>0.073492</td>
          <td>Invalid Material</td>
        </tr>
      </tbody>
    </table>
    </div>



By default, evaluation of a DataFrame of parameters will end when an
invalid combination is encountered.

.. code:: ipython3

    try:
        EvaluatorEP(
            problem,
            building,
            error_mode="Failfast",
            out_dir="outputdir",
            err_dir="outputdir",
        ).df_apply(samples)
    except Exception as e:
        print("caught", e)



.. parsed-literal::

    HBox(children=(FloatProgress(value=0.0, description='Executing', max=5.0, style=ProgressStyle(description_widt…


.. parsed-literal::

    Program Version,EnergyPlus, Version 9.0.1-bb7ca4f0da, YMD=2021.02.10 00:32,

       ************* Beginning Zone Sizing Calculations

       ** Severe  ** Did not find matching material for Construction EXT-SLAB, missing material = INVALID MATERIAL

       **  Fatal  ** GetSurfaceData: Errors discovered, program terminates.

       ...Summary of Errors that led to program termination:

       ..... Reference severe error count=1

       ..... Last severe error=Did not find matching material for Construction EXT-SLAB, missing material = INVALID MATERIAL

       ************* Warning:  Node connection errors not checked - most system input has not been read (see previous warning).

       ************* Fatal error -- final processing.  Program exited before simulations began.  See previous error messages.

       ************* EnergyPlus Warmup Error Summary. During Warmup: 0 Warning; 0 Severe Errors.

       ************* EnergyPlus Sizing Error Summary. During Sizing: 0 Warning; 1 Severe Errors.

       ************* EnergyPlus Terminated--Fatal Error Detected. 0 Warning; 1 Severe Errors; Elapsed Time=00hr 00min  0.30sec


    caught Command '['/usr/local/EnergyPlus-9-0-1/energyplus', '--idd', PosixPath('/usr/local/EnergyPlus-9-0-1/Energy+.idd'), '--weather', '/home/user/.local/lib/python3.7/site-packages/besos/data/example_epw.epw', '--output-directory', '/home/user/.besos_01t5vs48', PosixPath('/home/user/.besos_01t5vs48/in.epJSON')]' returned non-zero exit status 1.


.. parsed-literal::

    /home/user/.local/lib/python3.7/site-packages/besos/evaluator.py:191: UserWarning: for inputs: ['RangeParameter [0.01, 0.99]', "Category['HW CONCRETE', 'Invalid Material']"] problematic values were: RangeParameter [0.01, 0.99]                            0.735874
    Category['HW CONCRETE', 'Invalid Material']    Invalid Material
    Name: 2, dtype: object
      warn(msg)


However sometimes we want to have a fallback value for these invalid
states. For example, with an optimization algorithm that is minimizing,
we can set a very high output value. This can be specified with the
``error_value`` argument for evaluators. It must be of the form
``(objective_values, constraint_values)`` where objective\_values and
constraint\_values are tuples of the same length as the number of
objectives/constraints. Since we have 1 objective and no constraints, we
use a tuple with one item for objective\_values, and an empty tuple for
the constraint values.

.. code:: ipython3

    error_value = ((10.0 ** 20,), ())

    EvaluatorEP(problem, building, error_mode="Print", error_value=error_value).df_apply(
        samples
    )


.. parsed-literal::

    /home/user/.local/lib/python3.7/site-packages/besos/evaluator.py:394: FutureWarning: Evaluators have changed the format used for error values. The new format accepts either None or a tuple of values. See the evaluator docstring for details on the new format.
    Your error mode has automatically been converted to a new format equivalent.
      warnings.warn(warning, FutureWarning)



.. parsed-literal::

    HBox(children=(FloatProgress(value=0.0, description='Executing', max=5.0, style=ProgressStyle(description_widt…


.. parsed-literal::

    Program Version,EnergyPlus, Version 9.0.1-bb7ca4f0da, YMD=2021.02.10 00:32,

       ************* Beginning Zone Sizing Calculations

       ** Severe  ** Did not find matching material for Construction EXT-SLAB, missing material = INVALID MATERIAL

       **  Fatal  ** GetSurfaceData: Errors discovered, program terminates.

       ...Summary of Errors that led to program termination:

       ..... Reference severe error count=1

       ..... Last severe error=Did not find matching material for Construction EXT-SLAB, missing material = INVALID MATERIAL

       ************* Warning:  Node connection errors not checked - most system input has not been read (see previous warning).

       ************* Fatal error -- final processing.  Program exited before simulations began.  See previous error messages.

       ************* EnergyPlus Warmup Error Summary. During Warmup: 0 Warning; 0 Severe Errors.

       ************* EnergyPlus Sizing Error Summary. During Sizing: 0 Warning; 1 Severe Errors.

       ************* EnergyPlus Terminated--Fatal Error Detected. 0 Warning; 1 Severe Errors; Elapsed Time=00hr 00min  0.28sec


    Program Version,EnergyPlus, Version 9.0.1-bb7ca4f0da, YMD=2021.02.10 00:32,

       ************* Beginning Zone Sizing Calculations

       ** Severe  ** Did not find matching material for Construction EXT-SLAB, missing material = INVALID MATERIAL

       **  Fatal  ** GetSurfaceData: Errors discovered, program terminates.

       ...Summary of Errors that led to program termination:

       ..... Reference severe error count=1

       ..... Last severe error=Did not find matching material for Construction EXT-SLAB, missing material = INVALID MATERIAL

       ************* Warning:  Node connection errors not checked - most system input has not been read (see previous warning).

       ************* Fatal error -- final processing.  Program exited before simulations began.  See previous error messages.

       ************* EnergyPlus Warmup Error Summary. During Warmup: 0 Warning; 0 Severe Errors.

       ************* EnergyPlus Sizing Error Summary. During Sizing: 0 Warning; 1 Severe Errors.

       ************* EnergyPlus Terminated--Fatal Error Detected. 0 Warning; 1 Severe Errors; Elapsed Time=00hr 00min  0.29sec




.. parsed-literal::

    /home/user/.local/lib/python3.7/site-packages/besos/evaluator.py:191: UserWarning: for inputs: ['RangeParameter [0.01, 0.99]', "Category['HW CONCRETE', 'Invalid Material']"] problematic values were: RangeParameter [0.01, 0.99]                            0.580238
    Category['HW CONCRETE', 'Invalid Material']    Invalid Material
    Name: 3, dtype: object
      warn(msg)


.. parsed-literal::

    Program Version,EnergyPlus, Version 9.0.1-bb7ca4f0da, YMD=2021.02.10 00:32,

       ************* Beginning Zone Sizing Calculations

       ** Severe  ** Did not find matching material for Construction EXT-SLAB, missing material = INVALID MATERIAL

       **  Fatal  ** GetSurfaceData: Errors discovered, program terminates.

       ...Summary of Errors that led to program termination:

       ..... Reference severe error count=1

       ..... Last severe error=Did not find matching material for Construction EXT-SLAB, missing material = INVALID MATERIAL

       ************* Warning:  Node connection errors not checked - most system input has not been read (see previous warning).

       ************* Fatal error -- final processing.  Program exited before simulations began.  See previous error messages.

       ************* EnergyPlus Warmup Error Summary. During Warmup: 0 Warning; 0 Severe Errors.

       ************* EnergyPlus Sizing Error Summary. During Sizing: 0 Warning; 1 Severe Errors.

       ************* EnergyPlus Terminated--Fatal Error Detected. 0 Warning; 1 Severe Errors; Elapsed Time=00hr 00min  0.29sec





.. parsed-literal::

    /home/user/.local/lib/python3.7/site-packages/besos/evaluator.py:191: UserWarning: for inputs: ['RangeParameter [0.01, 0.99]', "Category['HW CONCRETE', 'Invalid Material']"] problematic values were: RangeParameter [0.01, 0.99]                           0.0734916
    Category['HW CONCRETE', 'Invalid Material']    Invalid Material
    Name: 4, dtype: object
      warn(msg)




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }

        .dataframe tbody tr th {
            vertical-align: top;
        }

        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Electricity:Facility</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>1.826067e+09</td>
        </tr>
        <tr>
          <th>1</th>
          <td>1.812593e+09</td>
        </tr>
        <tr>
          <th>2</th>
          <td>1.000000e+20</td>
        </tr>
        <tr>
          <th>3</th>
          <td>1.000000e+20</td>
        </tr>
        <tr>
          <th>4</th>
          <td>1.000000e+20</td>
        </tr>
      </tbody>
    </table>
    </div>



This time, we got a warning for the invalid states, and our error value
was used as the result. If we do not want to display these warnings, we
can set the ``error_mode='Silent'``. Omiting the error value will use a
reasonable default, set to the opposite of what we are optimizing each
objective towards. (This does not work for problems with constraints).

.. code:: ipython3

    evaluator = EvaluatorEP(problem, building, error_mode="Silent")
    print("Error value defaulted to:", evaluator.error_value)


.. parsed-literal::

    Error value defaulted to: [inf]


.. code:: ipython3

    evaluator.df_apply(samples)



.. parsed-literal::

    HBox(children=(FloatProgress(value=0.0, description='Executing', max=5.0, style=ProgressStyle(description_widt…


.. parsed-literal::






.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }

        .dataframe tbody tr th {
            vertical-align: top;
        }

        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Electricity:Facility</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>1.826067e+09</td>
        </tr>
        <tr>
          <th>1</th>
          <td>1.812593e+09</td>
        </tr>
        <tr>
          <th>2</th>
          <td>inf</td>
        </tr>
        <tr>
          <th>3</th>
          <td>inf</td>
        </tr>
        <tr>
          <th>4</th>
          <td>inf</td>
        </tr>
      </tbody>
    </table>
    </div>

Adaptive Sampling
=================

This notebook implements adaptive sampling, which explores under-sampled
parts of the design space while exploiting the knowledge gained from
previous simulation runs. The aim is to reduce the number of samples
required to train an accurate surrogate model. We use Lola-Voronoi based
sampling, which is compatible with any surrogate model type. Here it is
applied with a Gaussian Process model.

.. code:: ipython3

    import warnings

    import numpy as np
    import seaborn as sns
    from besos import eppy_funcs as ef, sampling
    from besos.evaluator import EvaluatorEP
    from besos.problem import EPProblem
    from dask.distributed import Client
    from matplotlib import pyplot as plt
    from scipy.interpolate import griddata
    from sklearn.gaussian_process import GaussianProcessRegressor
    from sklearn.gaussian_process.kernels import RBF, Matern, RationalQuadratic
    from sklearn.manifold import MDS
    from sklearn.model_selection import GridSearchCV, train_test_split
    from sklearn.preprocessing import StandardScaler

    from parameter_sets import parameter_set
    from sampling import adaptive_sampler_lv

.. code:: ipython3

    client = Client(threads_per_worker=1)
    client




.. raw:: html

    <table style="border: 2px solid white;">
    <tr>
    <td style="vertical-align: top; border: 0px solid white">
    <h3 style="text-align: left;">Client</h3>
    <ul style="text-align: left; list-style: none; margin: 0; padding: 0;">
      <li><b>Scheduler: </b>tcp://127.0.0.1:37212</li>
      <li><b>Dashboard: </b><a href='/user/peterrwilson99/proxy/8787/status' target='_blank'>/user/peterrwilson99/proxy/8787/status</a></li>
    </ul>
    </td>
    <td style="vertical-align: top; border: 0px solid white">
    <h3 style="text-align: left;">Cluster</h3>
    <ul style="text-align: left; list-style:none; margin: 0; padding: 0;">
      <li><b>Workers: </b>16</li>
      <li><b>Cores: </b>16</li>
      <li><b>Memory: </b>68.72 GB</li>
    </ul>
    </td>
    </tr>
    </table>



Define sampler settings
-----------------------

.. code:: ipython3

    n_samples_init = (
        20  # initial set of samples collected using the standard low-discrepancy sampling
    )

    no_iter = 10  # number of iterations of the adaptive sampler to run
    n = 4  # number of samples added per iteration

Generate data set
-----------------

This generates an example model and the initial sampling data, see `this
example <../../example-notebooks/How-to-Guides/FitGPModel.html>`__.

.. code:: ipython3

    parameters = parameter_set(7)
    problem = EPProblem(parameters, ["Electricity:Facility"])
    building = ef.get_building()
    inputs = sampling.dist_sampler(sampling.lhs, problem, n_samples_init)
    evaluator = EvaluatorEP(problem, building)
    outputs = evaluator.df_apply(inputs, processes=4)
    results = inputs.join(outputs)
    results.head()




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }

        .dataframe tbody tr th {
            vertical-align: top;
        }

        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Conductivity</th>
          <th>Thickness</th>
          <th>U-Factor</th>
          <th>Solar Heat Gain Coefficient</th>
          <th>ElectricEquipment</th>
          <th>Lights</th>
          <th>Window to Wall Ratio</th>
          <th>Electricity:Facility</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>0.068922</td>
          <td>0.278260</td>
          <td>0.589089</td>
          <td>0.808344</td>
          <td>11.278208</td>
          <td>12.836307</td>
          <td>0.114060</td>
          <td>1.994176e+09</td>
        </tr>
        <tr>
          <th>1</th>
          <td>0.165364</td>
          <td>0.199395</td>
          <td>2.800824</td>
          <td>0.907124</td>
          <td>10.950859</td>
          <td>10.328810</td>
          <td>0.502100</td>
          <td>1.845950e+09</td>
        </tr>
        <tr>
          <th>2</th>
          <td>0.159571</td>
          <td>0.249230</td>
          <td>0.290241</td>
          <td>0.980416</td>
          <td>14.532254</td>
          <td>11.521821</td>
          <td>0.884315</td>
          <td>2.152789e+09</td>
        </tr>
        <tr>
          <th>3</th>
          <td>0.083254</td>
          <td>0.112736</td>
          <td>1.924484</td>
          <td>0.297179</td>
          <td>14.368944</td>
          <td>12.232532</td>
          <td>0.804670</td>
          <td>2.119206e+09</td>
        </tr>
        <tr>
          <th>4</th>
          <td>0.028942</td>
          <td>0.203402</td>
          <td>4.931487</td>
          <td>0.079449</td>
          <td>11.817406</td>
          <td>14.989872</td>
          <td>0.679059</td>
          <td>2.103819e+09</td>
        </tr>
      </tbody>
    </table>
    </div>



Initial training of Surrogate Model
-----------------------------------

Here we use a Gaussian Process surrogate model, see
`here <../../example-notebooks/How-to-Guides/FitGPModel.html>`__ for details.

.. code:: ipython3

    train_in, test_in, train_out, test_out = train_test_split(
        inputs, outputs, test_size=0.2
    )

.. code:: ipython3

    hyperparameters = {
        "kernel": [
            None,
            1.0 * RBF(length_scale=1.0, length_scale_bounds=(1e-1, 10.0)),
            1.0 * RationalQuadratic(length_scale=1.0, alpha=0.5),
            # ConstantKernel(0.1, (0.01, 10.0))*(DotProduct(sigma_0=1.0, sigma_0_bounds=(0.1, 10.0))**2),
            1.0 * Matern(length_scale=1.0, length_scale_bounds=(1e-1, 10.0)),
        ]
    }

    folds = 3

    gp = GaussianProcessRegressor(normalize_y=True)

    clf = GridSearchCV(gp, hyperparameters, iid=True, cv=folds)
    with warnings.catch_warnings():
        warnings.simplefilter("ignore", category=FutureWarning)
        clf.fit(inputs, outputs)

    print(f"The best performing model $R^2$ score on the validation set: {clf.best_score_}")
    print(f"The model $R^2$ parameters: {clf.best_params_}")
    print(
        f"The best performing model $R^2$ score on a separate test set: {clf.best_estimator_.score(test_in, test_out)}"
    )
    reg = clf.best_estimator_


.. parsed-literal::

    The best performing model $R^2$ score on the validation set: 0.8932778938504393
    The model $R^2$ parameters: {'kernel': 1**2 * Matern(length_scale=1, nu=1.5)}
    The best performing model $R^2$ score on a separate test set: 1.0


LOLA - Voronoi sampling
-----------------------

Here we run our implementation of LOLA-Voronoi sampling. New designs to
be simulated are picked around previously simulated designs with a high
hybrid score :math:`H`.

:math:`H = V + E`

:math:`H` is used to incentives exploration :math:`V` and exploitation
:math:`E`. :math:`V` is the Voronoi cell size to approximate the sample
density and :math:`E` the local-linear estimate to approximate the
gradient in the neighbourhood of a sample.

.. code:: ipython3

    numiter = 10
    AS = adaptive_sampler_lv(
        train_in.values,
        train_out.values,
        n,
        problem,
        evaluator,
        reg,
        test_in,
        test_out,
        verbose=False,
    )
    AS.run(numiter)



.. parsed-literal::

    HBox(children=(FloatProgress(value=0.0, description='Executing', max=4.0, style=ProgressStyle(description_widt…


.. parsed-literal::


    0



.. parsed-literal::

    HBox(children=(FloatProgress(value=0.0, description='Executing', max=4.0, style=ProgressStyle(description_widt…


.. parsed-literal::


    1



.. parsed-literal::

    HBox(children=(FloatProgress(value=0.0, description='Executing', max=4.0, style=ProgressStyle(description_widt…


.. parsed-literal::


    2


.. parsed-literal::

    /home/user/.local/lib/python3.7/site-packages/sklearn/gaussian_process/_gpr.py:504: ConvergenceWarning: lbfgs failed to converge (status=2):
    ABNORMAL_TERMINATION_IN_LNSRCH.

    Increase the number of iterations (max_iter) or scale the data as shown in:
        https://scikit-learn.org/stable/modules/preprocessing.html
      _check_optimize_result("lbfgs", opt_res)



.. parsed-literal::

    HBox(children=(FloatProgress(value=0.0, description='Executing', max=4.0, style=ProgressStyle(description_widt…


.. parsed-literal::


    3



.. parsed-literal::

    HBox(children=(FloatProgress(value=0.0, description='Executing', max=4.0, style=ProgressStyle(description_widt…


.. parsed-literal::


    4



.. parsed-literal::

    HBox(children=(FloatProgress(value=0.0, description='Executing', max=4.0, style=ProgressStyle(description_widt…


.. parsed-literal::


    5



.. parsed-literal::

    HBox(children=(FloatProgress(value=0.0, description='Executing', max=4.0, style=ProgressStyle(description_widt…


.. parsed-literal::


    6



.. parsed-literal::

    HBox(children=(FloatProgress(value=0.0, description='Executing', max=4.0, style=ProgressStyle(description_widt…


.. parsed-literal::


    7



.. parsed-literal::

    HBox(children=(FloatProgress(value=0.0, description='Executing', max=4.0, style=ProgressStyle(description_widt…


.. parsed-literal::


    8



.. parsed-literal::

    HBox(children=(FloatProgress(value=0.0, description='Executing', max=4.0, style=ProgressStyle(description_widt…


.. parsed-literal::


    9



.. parsed-literal::

    HBox(children=(FloatProgress(value=0.0, description='Executing', max=4.0, style=ProgressStyle(description_widt…


.. parsed-literal::




.. parsed-literal::

    /home/user/.local/lib/python3.7/site-packages/sklearn/gaussian_process/_gpr.py:504: ConvergenceWarning: lbfgs failed to converge (status=2):
    ABNORMAL_TERMINATION_IN_LNSRCH.

    Increase the number of iterations (max_iter) or scale the data as shown in:
        https://scikit-learn.org/stable/modules/preprocessing.html
      _check_optimize_result("lbfgs", opt_res)


Visualization
-------------

We visualize the working of the adaptive sampler by reducing the input
dimensionality to 2 using multi-dimensional scaling.

.. code:: ipython3

    plt.plot(range(n_samples_init, n_samples_init + no_iter * n, n), AS.score[:-1], "-o")
    plt.ylabel("R^2 score")
    plt.xlabel("No. of samples")




.. parsed-literal::

    Text(0.5, 0, 'No. of samples')




.. image:: FitGPAdaptive_files/FitGPAdaptive_14_1.png


.. code:: ipython3

    # Manifold model
    scaler_mds = StandardScaler()
    p_norm = scaler_mds.fit_transform(AS.P)
    model = MDS(n_components=2, random_state=1)
    out3 = model.fit_transform(p_norm)

    plt.scatter(out3[:n_samples_init, 0], out3[:n_samples_init, 1], color="grey")
    plt.scatter(out3[n_samples_init:, 0], out3[n_samples_init:, 1], color="r")
    plt.axis("equal")




.. parsed-literal::

    (-4.207227499374288, 4.505355347028606, -3.7690514453462707, 4.057620752975868)




.. image:: FitGPAdaptive_files/FitGPAdaptive_15_1.png


.. code:: ipython3

    ax = sns.kdeplot(out3[n_samples_init:, 0], out3[n_samples_init:, 1], shade=True)
    plt.ylim([-4, 4])
    plt.xlim([-4, 4])


.. parsed-literal::

    /usr/local/lib/python3.7/dist-packages/seaborn/_decorators.py:43: FutureWarning: Pass the following variable as a keyword arg: y. From version 0.12, the only valid positional argument will be `data`, and passing other arguments without an explicit keyword will result in an error or misinterpretation.
      FutureWarning




.. parsed-literal::

    (-4.0, 4.0)




.. image:: FitGPAdaptive_files/FitGPAdaptive_16_2.png


.. code:: ipython3

    x = out3[:, 0]
    y = out3[:, 1]
    z = reg.predict(AS.P)

    # Convert from pandas dataframes to numpy arrays
    X, Y, Z, = np.array([]), np.array([]), np.array([])
    for i in range(len(x)):
        X = np.append(X, x[i])
        Y = np.append(Y, y[i])
        Z = np.append(Z, z[i])

    # create x-y points to be used in heatmap
    xi = np.linspace(X.min(), X.max(), 1000)
    yi = np.linspace(Y.min(), Y.max(), 1000)

    # Z is a matrix of x-y values
    zi = griddata((X, Y), Z, (xi[None, :], yi[:, None]), method="cubic")

    # I control the range of my colorbar by removing data
    # outside of my range of interest
    zmin = 1.0 * 10 ** 9
    zmax = 3 * 10 ** 9
    zi[(zi < zmin) | (zi > zmax)] = None

    # Create the contour plot
    plt.figure(figsize=(10, 10))
    CS = plt.contourf(xi, yi, zi, 10, cmap=plt.cm.rainbow, vmax=zmax, vmin=zmin, alpha=0.8)
    plt.colorbar()
    plt.scatter(out3[:18, 0], out3[:18, 1], color="grey")
    plt.scatter(out3[18:, 0], out3[18:, 1], color="r")
    # ax = sns.kdeplot(out3[18:,0], out3[18:,1], shade=True)


    plt.ylim([-4, 4])
    plt.xlim([-4, 4])
    plt.grid()


.. parsed-literal::

    /usr/local/lib/python3.7/dist-packages/ipykernel_launcher.py:23: RuntimeWarning: invalid value encountered in less
    /usr/local/lib/python3.7/dist-packages/ipykernel_launcher.py:23: RuntimeWarning: invalid value encountered in greater



.. image:: FitGPAdaptive_files/FitGPAdaptive_17_1.png

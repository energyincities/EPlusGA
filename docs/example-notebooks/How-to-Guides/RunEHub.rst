Running EHub on BESOS
=====================

This notebook loads an Excel-based model and runs it. You can make an
``Excel (.xlsx)`` input file by following the format of
``test_file.xlsx``. An overview of the model is given `here <#TODO>`__.

.. code:: ipython3

    from pyehub.energy_hub.ehub_model import EHubModel
    from pyehub.outputter import pretty_print

.. code:: ipython3

    excel_file = (
        "test_file.xlsx"  # name of the excel file. [This must be in the current directory]
    )
    my_model = EHubModel(
        excel=excel_file
    )  # instantiate our model. Nothing is solved at this point.
    results = my_model.solve()  # solve the model and get back our results
    pretty_print(results)  # print the results to the console


.. parsed-literal::

    Version: 0.1.0
    Solver
    	termination condition: Optimal
    	time: 0.07186318002641201
    Solution

    ========== Stuff ==========

    ANNUAL_MAINTENANCE_STORAGE_COSTS:
                    ANNUAL_MAINTENANCE_STORAGE_COSTS
    Battery                                        0
    Hot Water Tank                                 0

    BIG_M:
    99999

    Battery:
    0.0

    Boiler:
    10.0

    CARBON_CREDITS:
                CARBON_CREDITS
    Elec
    Gas
    Grid
    Heat
    Irradiation
    PV_Elec                0.3

    CARBON_FACTORS:
                CARBON_FACTORS
    Elec
    Gas                  0.194
    Grid                  0.35
    Heat
    Irradiation
    PV_Elec

    CHARGING_EFFICIENCY:
                    CHARGING_EFFICIENCY
    Battery                        0.99
    Hot Water Tank                 0.80

    CHP:
    5.0

    CONVERSION_EFFICIENCY:
              Elec  ...  PV_Elec
    Boiler     0.0  ...    0.000
    CHP        0.3  ...    0.000
    GSHP      -1.0  ...    0.000
    Grid       1.0  ...    0.000
    HP        -1.0  ...    0.000
    MicroCHP   0.9  ...    0.000
    PV         0.0  ...    0.165
    ST         0.0  ...    0.000

    [8 rows x 6 columns]

    DISCHARGING_EFFICIENCY:
                    DISCHARGING_EFFICIENCY
    Battery                           0.99
    Hot Water Tank                    0.40

    FEED_IN_TARIFFS:
             FEED_IN_TARIFFS
    PV_Elec              4.0

    FIXED_CAPITAL_COSTS:
              FIXED_CAPITAL_COSTS
    Boiler                      0
    CHP                         0
    GSHP                        0
    Grid                        0
    HP                          0
    MicroCHP                    0
    PV                          0
    ST                          0

    FIXED_CAPITAL_COSTS_STORAGE:
                    FIXED_CAPITAL_COSTS_STORAGE
    Battery                                   0
    Hot Water Tank                            0

    FUEL_PRICES:
                FUEL_PRICES
    Elec
    Gas               0.028
    Grid               0.13
    Heat
    Irradiation
    PV_Elec

    GSHP:
    0.0

    Grid:
    2.24958

    HP:
    0.0

    Hot Water Tank:
    26.3417

    LINEAR_CAPITAL_COSTS:
              LINEAR_CAPITAL_COSTS
    Boiler                   500.0
    CHP                     1000.0
    GSHP                   15000.0
    Grid                       0.0
    HP                      1500.0
    MicroCHP                1800.0
    PV                      6000.0
    ST                      4500.0

    LINEAR_STORAGE_COSTS:
                    LINEAR_STORAGE_COSTS
    Battery                       800.00
    Hot Water Tank                  1.33

    LOADS:
        Elec  Heat
    0    1.0  20.0
    1    4.0  20.0
    2    4.0  20.0
    3    4.0  20.0
    4    4.0  20.0
    5    4.0  20.0
    6    4.0  20.0
    7    4.0  12.0
    8    4.0  12.0
    9    4.0  12.0
    10   4.0  12.0

    MAX_CARBON:
    None

    MAX_CHARGE_RATE:
                    MAX_CHARGE_RATE
    Battery                     0.3
    Hot Water Tank              0.3

    MAX_DISCHARGE_RATE:
                    MAX_DISCHARGE_RATE
    Battery                        0.3
    Hot Water Tank                 0.3

    MIN_STATE_OF_CHARGE:
                    MIN_STATE_OF_CHARGE
    Battery                         0.0
    Hot Water Tank                  0.0

    MicroCHP:
    1.94491

    NET_PRESENT_VALUE_STORAGE:
                    NET_PRESENT_VALUE_STORAGE
    Battery                          0.101852
    Hot Water Tank                   0.101852

    NET_PRESENT_VALUE_TECH:
              NET_PRESENT_VALUE_TECH
    Boiler                    0.0888
    CHP                       0.1019
    GSHP                      0.0817
    Grid                      0.0800
    HP                        0.1019
    MicroCHP                  0.1019
    PV                        0.1019
    ST                        0.0858

    OMV_COSTS:
              OMV_COSTS
    Boiler         0.01
    CHP            0.03
    GSHP           0.10
    Grid           0.00
    HP             0.10
    MicroCHP       0.02
    PV             0.01
    ST             0.01

    PART_LOAD:
              Elec  Heat
    CHP        0.5   NaN
    MicroCHP   0.5   0.5

    PV:
    1.0

    ST:
    0.0

    STORAGE_STANDING_LOSSES:
                    STORAGE_STANDING_LOSSES
    Battery                           0.001
    Hot Water Tank                    0.200

    TIME_SERIES:
        Elec  Heat  Irradiation
    0    1.0  20.0        0.000
    1    4.0  20.0        0.019
    2    4.0  20.0        0.052
    3    4.0  20.0        0.092
    4    4.0  20.0        0.115
    5    4.0  20.0        0.116
    6    4.0  20.0        0.095
    7    4.0  12.0        0.057
    8    4.0  12.0        0.006
    9    4.0  12.0        0.000
    10   4.0  12.0        0.000

    capacities:
             capacities
    Boiler       Boiler
    CHP             CHP
    GSHP           GSHP
    Grid           Grid
    HP               HP
    MicroCHP   MicroCHP
    PV               PV
    ST               ST

    capacity_storage:
                    capacity_storage
    Battery                   0.0000
    Hot Water Tank           26.3417

    capacity_tech:
              capacity_tech
    Boiler         10.00000
    CHP             5.00000
    GSHP            0.00000
    Grid            2.24958
    HP              0.00000
    MicroCHP        1.94491
    PV              1.00000
    ST              0.00000

    demands:
    ['Elec', 'Heat']

    energy_exported:
         PV_Elec
    0   0.000000
    1   0.003135
    2   0.008580
    3   0.015180
    4   0.018975
    5   0.019140
    6   0.015675
    7   0.009405
    8   0.000990
    9   0.000000
    10  0.000000

    energy_from_storage:
        Battery  Hot Water Tank
    0       0.0         4.60000
    1       0.0         0.09749
    2       0.0         0.09749
    3       0.0         0.09749
    4       0.0         0.09749
    5       0.0         0.09749
    6       0.0         0.09749
    7       0.0         0.00000
    8       0.0         0.00000
    9       0.0         0.00000
    10      0.0         0.00000

    energy_imported:
            Gas     Grid
    0   11.1111  0.00000
    1   11.9449  2.24958
    2   11.9449  2.24958
    3   11.9449  2.24958
    4   11.9449  2.24958
    5   11.9449  2.24958
    6   11.9449  2.24958
    7   11.9449  2.24958
    8   11.9449  2.24958
    9   11.9449  2.24958
    10  11.9449  2.24958

    energy_input:
        Boiler  ...   ST
    0     10.0  ...  0.0
    1     10.0  ...  0.0
    2     10.0  ...  0.0
    3     10.0  ...  0.0
    4     10.0  ...  0.0
    5     10.0  ...  0.0
    6     10.0  ...  0.0
    7     10.0  ...  0.0
    8     10.0  ...  0.0
    9     10.0  ...  0.0
    10    10.0  ...  0.0

    [11 rows x 8 columns]

    energy_to_storage:
        Battery  Hot Water Tank
    0       0.0         0.00000
    1       0.0         0.00000
    2       0.0         0.00000
    3       0.0         0.00000
    4       0.0         0.00000
    5       0.0         0.00000
    6       0.0         0.00000
    7       0.0         7.90251
    8       0.0         7.90251
    9       0.0         7.90251
    10      0.0         7.90251

    export_streams:
    ['PV_Elec']

    import_streams:
    ['Grid', 'Gas']

    investment_cost:
    1925.2

    is_installed:
              is_installed
    Boiler               1
    CHP                  1
    GSHP                 0
    Grid                 1
    HP                   0
    MicroCHP             1
    PV                   1
    ST                   0

    is_installed_2:
                    is_installed_2
    Battery                      0
    Hot Water Tank               1

    is_on:
        CHP  MicroCHP
    0     0         1
    1     0         1
    2     0         1
    3     0         1
    4     0         1
    5     0         1
    6     0         1
    7     0         1
    8     0         1
    9     0         1
    10    0         1

    maintenance_cost:
    3.6255

    operating_cost:
    6.21582

    output_streams:
    ['Elec', 'Heat', 'PV_Elec']

    part_load:
    ['MicroCHP', 'CHP']

    sources:
    ['Irradiation']

    storage_capacity:
                   storage_capacity
    Battery                 Battery
    Hot Water Tank   Hot Water Tank

    storage_level:
        Battery  Hot Water Tank
    0       0.0       18.662600
    1       0.0        3.430050
    2       0.0        2.500320
    3       0.0        1.756530
    4       0.0        1.161500
    5       0.0        0.685475
    6       0.0        0.304656
    7       0.0        0.000000
    8       0.0        6.322010
    9       0.0       11.379600
    10      0.0       15.425700
    11      0.0       18.662600

    storages:
    ['Battery', 'Hot Water Tank']

    stream_timeseries:
                stream_timeseries
    Irradiation       Irradiation

    streams:
    ['Elec', 'Heat', 'Irradiation', 'Grid', 'Gas', 'PV_Elec']

    technologies:
    ['Grid', 'HP', 'Boiler', 'MicroCHP', 'PV', 'ST', 'CHP', 'GSHP']

    time:
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    total_carbon:
    33.1749

    total_cost:
    1935.04
